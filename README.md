Generates sound of a guitar string. 

In order to do that, there's being used the wave equation solution (that uses Fourier series).
Sample sound is recorded in the sound.wav file.

By some reason, depending on parameters, the sound is either 'flat' (like the currently recorded one) or really rough and is never like real guitar sound.
Current hypothesis is that there isn't enough accuracy in float value to listen all the overtones that we hear from a real guitar.
