#include <math.h>
#include <string.h>

double integral(math_func func,
				double a, 
				double b, 
				int num_of_partition_sub_intervals) 
{

	double delta_x = (b - a) / num_of_partition_sub_intervals;

	double result = 0;
	for (double x = a; x < b; x += delta_x) {
		result += delta_x * func(x);
	}

	return result;
}

double scalar_multiplication(math_func lhs, 
							 math_func rhs,
							 double a, 
							 double b, 
							 int num_of_partition_sub_intervals) 
{

	auto mult = [rhs, lhs] (double x) -> double { 
											return rhs(x)*lhs(x);
									 	};

	return integral (mult, a, b, num_of_partition_sub_intervals);
}

std::vector<double> fourier_series_coefficients(std::function<double(double)> func,
												double x_0,
												double x_1,
												int num_of_partition_sub_intervals,
												int num_of_waves)
{
	std::vector<double> result;
	for (int n = 1; n <= num_of_waves; n++) {
		math_func e_n = [x_0, x_1, n] (double x) { 
							return sin(M_PI * n * x / (x_1 - x_0) - x_0); 
						};

		result.push_back(
					 2 / (x_1 - x_0) * scalar_multiplication(func,
										   		e_n,
										   		x_0,
								           		x_1,
								           		num_of_partition_sub_intervals
								        )
				);
	}

	return result;	
}
