#include <string.h>
#include <math.h>

/* constructors */
String::String
        (
            math_func default_waveform,
            int prtn_sub_intervals_num,
            int num_of_waves,
            double length,
            double tension,
            double density
        ):

    length(length),
    tension(tension),
    density(density),
    prtn_sub_intervals_num(prtn_sub_intervals_num),
    string_coefficient(sqrt(tension / density)),
    frequency(2 * length / string_coefficient)

{
    fourier_coefficients = fourier_series_coefficients
                           (
                                    default_waveform, 
                                    0,
                                    length,
                                    prtn_sub_intervals_num,
                                    num_of_waves
                           );
    
}


String::String
        (
            std::vector<double> fourier_coefficients,
            int prtn_sub_intervals_num,
            int num_of_waves,
            double length,
            double tension,
            double density
        ): 

    length(length),
    tension(tension),
    density(density),
    prtn_sub_intervals_num(prtn_sub_intervals_num),
    string_coefficient(sqrt(tension / density)),
    fourier_coefficients(fourier_coefficients),
    frequency(2 * length / string_coefficient)

{}


/* output */
double String::string_waveform(double x, double t) {

    double a = string_coefficient;
    
    if (x < 0 || x > length) {
        return 0;
        printf("Troubles\n"); //КОСТЫЛЬ
    }

    double result = 0;
    for (int i = 0; i < fourier_coefficients.size(); i++) {
        int n = i + 1;
        result += fourier_coefficients.at(i) * 
                  cos(a * M_PI * n * t / length) *
                  sin(M_PI * n * x / length);
    }

    return result;
}

double String::sound_waveform(double t) {

    double a = string_coefficient;

    double result = 0;
    for (int i = 0; i < fourier_coefficients.size(); i++) {
        int n = i + 1;
        result += fourier_coefficients.at(i) * M_PI * n / 
                  length * cos(a * M_PI * n * t / length);
    }

    return result;
}

/* settings */
void String::set_tension(double new_tension) {
    tension = new_tension;
    string_coefficient = sqrt(density / tension);
}

void String::set_string_coefficient(double new_coefficient)
{
    string_coefficient = new_coefficient;
    density = 1;
    tension = pow(string_coefficient, 2);
}

void String::set_waveform(
        math_func default_waveform,
        double num_of_waves
    )

{
    fourier_coefficients = fourier_series_coefficients
                           (
                                default_waveform, 
                                0,
                                length,
                                prtn_sub_intervals_num,
                                num_of_waves
                           );    

}


/* visible output */
void String::draw_frame(draw_line_func draw_line,
                double t,
                double x0,
                double y0,
                double x_stretch,
                double y_stretch)
{
    /* real coordinates of small part of string */
    double previous_x = 0, current_x = 0;
    double previous_y = 0, current_y = 0;

    for (int i = 1; i <= prtn_sub_intervals_num; i++) {
        current_x = i * length / prtn_sub_intervals_num;
        current_y = string_waveform(current_x, t);

        draw_line(x0 + x_stretch * previous_x,
                  y0 + y_stretch * previous_y,
                  x0 + x_stretch * current_x,
                  y0 + y_stretch * current_y);

        previous_x = current_x;
        previous_y = current_y;
    }

}

void String::oscillogram(
        draw_line_func draw_line,
        double t,
        double x0,
        double y0,
        double x_stretch,
        double y_stretch,
        int sampling
)
{
    double previous_time = 0, current_time = 0;
    double previous_sound = 0, current_sound = 0;

    //printf("~~%f\n", t * sampling);
    for (int i = 0; i < t * sampling; i++) {

        current_time = (double)i / sampling;
        current_sound = sound_waveform(current_time);

        draw_line(x0 + x_stretch * previous_time,
                  y0 + y_stretch * previous_sound,
                  x0 + x_stretch * current_time,
                  y0 + y_stretch * current_sound);

        previous_time = current_time;
        previous_sound = current_sound;
    }
}

void String::real_time_oscillogram(
        draw_line_func draw_line,
        double t,
        double dt,
        double x0,
        double y0,
        double x_stretch,
        double y_stretch,
        int sampling
    )
{
    double previous_time = 0, current_time = 0;
    double previous_sound = 0, current_sound = 0;

    for (int i = 0; i < dt * sampling; i++) {

        current_time = (double)i / sampling + t;
        current_sound = sound_waveform(current_time);

        draw_line(x0 + x_stretch * previous_time,
                  y0 + y_stretch * previous_sound,
                  x0 + x_stretch * current_time,
                  y0 + y_stretch * current_sound);

        previous_time = current_time;
        previous_sound = current_sound;

    }
}
