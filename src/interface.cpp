#include <interface.h>
#include <SFML/Graphics.hpp>
#include <stdlib.h>

void display(String *string, double time_dilation) {

	sf::Clock clock;

	sf::RenderWindow window(sf::VideoMode(600, 600), "String");

	draw_line_func draw_line = [&window]
					(double x0, double y0,
			   		 double x1, double y1) -> double
	{
		sf::Vertex line[] = {
			sf::Vertex(sf::Vector2f(x0, y0)),
			sf::Vertex(sf::Vector2f(x1, y1))
		};


		window.draw(line, 2, sf::Lines);
	};


	int b = 1, counter = 0;
	while(window.isOpen()) {

		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}

		double time = clock.getElapsedTime().asSeconds() /
						time_dilation;

		//string.draw_frame(draw_line, time, 90., 200., 5000., 5000.);
		//string->draw_frame(draw_line, time, 5., 300., (600. - 10.)/string->length, (600. - 10.)/string->length);

		string->oscillogram(draw_line, time, 5., 300., 
							(600 - 10.) / string->frequency / 10,
							6000,
							10000
							);

		window.display();
		window.clear();

	}

}

void write_wav(String* string,
			   const char* filename,
			   double volume) {

	FILE* file = wavfile_open(filename);

	double duration = 3.;
	int length = WAVFILE_SAMPLES_PER_SECOND * duration;
	short* data = (short*)malloc(sizeof(short) * length);


	for (int i = 0; i < length; i++) {
		double time = (double)i / WAVFILE_SAMPLES_PER_SECOND;
		double input = string->sound_waveform(time) * volume;
		if (input < 16000) {
			data[i] = input;
		} else {
			printf("%f\n", input);
			break;
		}
		//printf("%f\n", string->sound_waveform(time));
		//data[i] = volume * sin(440 * 2 * M_PI * time);
	}

	wavfile_write(file, data, length);
	wavfile_close(file);

	free(data);
}