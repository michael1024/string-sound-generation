LIBS           = lib/Wavfile/wavfile.a
INC_LIBS_FLAGS = -I./lib/Wavfile/

EXTERNAL_LIBS = -lsfml-window -lsfml-system -lsfml-graphics

OBJS = main.o fourier_series.o string.o interface.o

CXX = g++
CXX_FLAGS = -std=c++17 -I./include/ $(INC_LIBS_FLAGS) $(EXTERNAL_LIBS)



main: $(LIBS) $(OBJS)
	$(CXX) -o main $(OBJS) $(CXX_FLAGS) $(LIBS)


#LIBS
lib/Wavfile/wavfile.a:
	(cd lib/Wavfile; $(MAKE))


#OBJS
main.o: main.cpp
	$(CXX) -c main.cpp $(CXX_FLAGS) $(INC_LIBS_FLAGS)

fourier_series.o: src/fourier_series.cpp
	$(CXX) -c src/fourier_series.cpp $(CXX_FLAGS) $(INC_LIBS_FLAGS)

string.o: src/string.cpp
	$(CXX) -c src/string.cpp $(CXX_FLAGS) $(INC_LIBS_FLAGS) 

interface.o: src/interface.cpp
	$(CXX) -c src/interface.cpp $(CXX_FLAGS) $(INC_LIBS_FLAGS) 

#PHONY
clean:
	-rm *.o main

cleanall:
	(cd lib/Wavfile/; $(MAKE) clean)
	$(MAKE) clean
