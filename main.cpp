#include <stdio.h>
#include <cmath>

#include <string.h>
#include <wavfile.h>
#include <interface.h>


const int PRTN_SUB_INTERVALS = 1; //50000
const int NUM_OF_WAVES = 100;

const double DELTA_H = 0.01;

const double C1_FREQUENCY = 440.;

const double LENGTH = 0.65;
const double TENSION = pow(2 * LENGTH * C1_FREQUENCY, 2);
const double DENSITY = 1;

const double TIME_DILATION = 1;

double default_waveform(double x) {
	return -(2*DELTA_H / LENGTH * std::abs(x - LENGTH/2)) +
			DELTA_H;
}

int main() {

	String* string = new String(
							default_waveform,
							PRTN_SUB_INTERVALS,
							NUM_OF_WAVES,
							LENGTH,
							TENSION,
							DENSITY
					);

	for (int i = 0; i < NUM_OF_WAVES; i++) {
		double inp;
		scanf("%lf", &inp);
		if (inp != 8)
			string->fourier_coefficients[i] = inp;
		else
			break;
	}

	display(string, TIME_DILATION);
	//write_wav(string, "sound.wav", 16000.); //1600000 для струны (костыль костылей), 16000 для функций с амплитудой 1

	delete string;

	return 0;
}
