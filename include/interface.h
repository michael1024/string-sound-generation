#ifndef INTERFACE_H
#define INTERFACE_H

#include <string.h>
#include <wavfile.h>
#include <stdio.h>

void display(String* string, double time_dilation);
void write_wav(String* string,
			   const char* filename,
			   double volume);

#endif