#ifndef FOURIER_SERIES_H
#define FOURIER_SERIES_H

#include <functional>
#include <vector>


typedef std::function<double(double)> math_func;

double integral(math_func func, 
				double a, 
				double b, 
				int num_of_partition_sub_intervals);

double scalar_multiplication(math_func lhs, 
							 math_func rhs, 
							 double a, 
							 double b, 
							 int num_of_partition_sub_intervals);

std::vector<double> fourier_series_coefficients(
                    math_func func,
                    double x0,
                    double x1,
                    int num_of_partition_sub_intervals,
                    int num_of_waves);

#endif