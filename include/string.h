#ifndef STRING_H
#define STRING_H

#include <vector>
#include <string>
#include <fourier_series.h> 



/* Первые два числа -- координаты первой точки,
   вторые два -- второй. */
typedef std::function<void(double, double, 
                           double, double)> draw_line_func;


class String {

public:

    double length; /* Длина колеблющийся части струны в м
                      (0.65 для моей гитары). */

    double tension; /* Натяжение колеблющийся части струны
                       в Па. */

    double density; /* Плотность колеблющийся части струны
                     * в кг/м. Замечу, что она меньше, чем у
                     * ненатянутой. */

    /* Пусть волновое ур-е -- 
     * d^2/dt^2 * h = a^2 * d^2/dx^2 h,
     * (h -- отклонение струны в м), 
     * тогда a -- string_coefficient. 
     * Он равен sqrt(tension / density).
     */
    double string_coefficient; 

    double frequency;

    int prtn_sub_intervals_num;
    std::vector<double> fourier_coefficients;


    String(std::function<double(double)> default_waveform,
           int prtn_sub_intervals_num,
           int num_of_waves,
           double length,
           double tension,
           double density);

    String(std::vector<double> fourier_coefficients,
           int prtn_sub_intervals_num,
           int num_of_waves,
           double length,
           double tension,
           double density);



    void set_tension(double new_tension);

    void set_string_coefficient(double new_coefficient);

    void set_waveform(math_func new_waveform, 
                      double num_of_waves);

    /* Отклонение каждой точки струны в зависимости
       от времени метрах. */
    double string_waveform(double x, double t); 
    
    double sound_waveform(double t);


    /* Рисует струну с помощью функции draw_line.
       В одной единице измерения draw_line один метр. */
    void draw_frame(draw_line_func draw_line,
                    double t,
                    double x0,
                    double y0,
                    double x_stretch,
                    double y_stretch);

    /* Рисует осциллограмму от t до t + dt. */
    void oscillogram(
            draw_line_func draw_line,
            double t,
            double x0,
            double y0,
            double x_stretch,
            double y_stretch,
            int sampling
        );

    void real_time_oscillogram(
            draw_line_func draw_line,
            double t,
            double dt,
            double x0,
            double y0,
            double x_stretch,
            double y_stretch,
            int sampling
        );

};

#endif
